#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
//Complete the following function.


void calculate_the_maximum(int n, int k) {
  int a, b, v1, v2, andMax = 0, orMax = 0, xorMax = 0, andMaxT, orMaxT, xorMaxT, var1;
  for(v1 = 1; v1 <= k; v1++) {
    
    for(v2 = (v1+1); v2 <= n; v2++) {

        andMaxT = v1 & v2;
        orMaxT = v1 | v2;
        xorMaxT = v1 ^ v2;

        if(andMaxT > andMax && andMaxT < k) andMax = andMaxT;
        if(orMaxT > orMax && orMaxT < k) orMax = orMaxT;
        if(xorMaxT > xorMax && xorMaxT < k) xorMax = xorMaxT;
    }
  }
  printf("%d\n", andMax);
  printf("%d\n", orMax);
  printf("%d\n", xorMax);
}

int main() {
    int n, k;
  
    scanf("%d %d", &n, &k);
    calculate_the_maximum(n, k);
 
    return 0;
}