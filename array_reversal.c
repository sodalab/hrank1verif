#include <stdio.h>
#include <stdlib.h>

int main()
{
    int num, *arr, i, i2, tempNum;
    scanf("%d", &num);
    arr = (int*) malloc(num * sizeof(int));
    for(i = 0; i < num; i++) {
        scanf("%d", arr + i);
    }
    int *tempArr[num];
    
    tempNum = num;
    for(i = 0; i < num; i++) {
        tempArr[num - 1 - i] = arr[i];
    }
    for(i = 0; i < num; i++) {
        arr[i] = tempArr[i];
    }

    for(i = 0; i < num; i++)
        printf("%d ", *(tempArr + i));
    return 0;
}
